#!/usr/bin/env zsh

#-------------#
# KDE log out #
#-------------#

qdbus org.kde.ksmserver /KSMServer logout 0 0 0

exit 0
